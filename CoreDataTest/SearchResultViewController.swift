//
//  SearchResultViewController.swift
//  CoreDataTest
//
//  Created by Santosh Tekulapally on 2018-11-12.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit
import CoreData

class SearchResultViewController: UIViewController {
    @IBOutlet var namelbl: UILabel!
    
    @IBOutlet var passwordlbl: UILabel!
    var personName:String="";
    var context:NSManagedObjectContext!


    override func viewDidLoad() {
        super.viewDidLoad()

        
        print ("step two")
        
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // 2. Mandatory - initialize the context variable
        // This variable gives you access to the CoreData functions
        self.context = appDelegate.persistentContainer.viewContext

        let fetchRequest:NSFetchRequest<User> = User.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "email==%@", self.personName)
        do {
            
            let results = try self.context.fetch(fetchRequest) as [User]
            
            // Loop through the database results and output each "row" to the screen
            print("Number of items in database: \(results.count)")
            
            
            if(results.count == 1){
                let x = results[0] as User
                namelbl.text = x.email!
                passwordlbl.text = x.password!
                
            }
            
            else if (results.count == 0){
                namelbl.text="error no results found"
            }
            else if(results.count > 1 ){
                passwordlbl.text = "eroor no results found"
            }
//            for x in results {
//                print("User Email: \(x.email)")
//                print("User Password: \(x.password)")
            }
        
        catch {
            print("Error when fetching from database")
        }

        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
